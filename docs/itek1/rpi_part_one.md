# Configure the Raspberry Pi for Study

In the study of Network and embedded technology we use the Raspberry Pi (RPi) almost exclusively as a tool. It is our go-to development platform and we learn and practice network commands on it. At the end of the semester, this development platform will also be the center of project 2, more information will come during the semester. But before we can begin all the fun and practice, we need to configure this tool and get acquainted with all the possibilities.

## Introduction

In this how-to guide and lab we will begin our journey of endless hours of fun working with the Raspberry Pi. The Raspberry Pi is a well known Single Board Computer (SBC) and is manufactured and developed by the UK-based charity organisation “The Raspberry Pi Foundation”. The Organisation works to put the power of computing and digital making into hands of people all over the world. THis is done in order to help people harness the power of computing and digital problem solving. According to their latest annual review (2019) Raspberry Pi computers have sold over 30 million devices.
What do you need


To follow this guide you will of course need your laptop and Raspberry Pi (RPi), but you will also need some other things in your group. 

- Laptop (Win / mac / linux)
- Raspberry Pi
- SD-card
- SD-card reader (external or internal in your laptop)
- 10 ethernet cables
- Network Switch
    - 1 x Linksys LGS308 8-port switch
    - 1 x Netgear GS108 8-port switch
- 1 x Pre-configured DHCP server ( Rpi in White case)
- Power connector for all your devices

---

## What do we want

It is very important that you help each other in the group and follow along all together in setting up each device. This is because before you know it, you will perform this operation yourself and on 2. And 3. Semester you will deploy a lot of server instances on the schools test network so we better get practise setting up servers.

When the group is finished with this guide you should all be able to connect to your RPi remotely in your little group network and perform relevant network diagnostic commands.

We will first start with a small Local Area Network (LAN) without access to other networks like the internet. When we are comfortable that everything is working, we will connect your network to the internet and get connectivity to the World (and the World Wide Web). Therefore this guide will consist of three parts:

**PartOne** is about getting an Operating System installed on your Raspberry Pi and getting it operational and ready to connect to a network. Using and practicing network commands on your laptop and on the RPi. **PartTwo** is about learning basic networking commands and Linux commands in relation to network and configuration of the Operating System. You will work with different diagnostic tools in order to investigate the network. You will configure and maintain the local DHCP server. **PartThree** is about connecting your network to another groups network and the ITEK 1st network on campus.

!!! note "Internet meaning"
    *an electronic communications network that connects computer networks and organizational computer facilities
    around the world —used with the except when being used attributively*
    **Marriam-Webster**

## Operating system
What is an Operating System? We sometimes also categorize it as System Software in contrast to Application software. In programming lessons at this education you will learn more about Application software and how to develop applications for everyday tasks. We can think of the operating system as the interface between a user and the computer hardware. IT performs all the basic processes like file management, memory management, handling input and output and controlling peripheral devices.

A modern computer as the one that your are using to read this information will most definitely consists of one or more processors, some main memory,
disks, printers, a keyboard, a mouse, a display, network interfaces, and various
other peripheral input/output devices. This is indeed a very complex system.

To manage all these components and use them optimally is an exceedingly challenging job and this is where the Operating system enters the stage. Computers are installed with a layer of system software called the operating system. The job of the OS is to provide user programs (Application software) with a better, simpler, cleaner, model of the computer and to handle managing all the resources just mentioned.

You will most definitely have some experience with an operating system such as Windows, Linux, FreeBSD, or OS X.


![OS abstractation](../img/itek1/01_fig_operating_system.png)


If you have tried to interact with your operating system in text form, you have used what we call a shell, if you only have used icons and the mouse, we call it a GUI (Graphical User Interface)—which is pronounced ‘‘gooey’’. But neither of these is actually part of the operating system, although it uses the operating system to get its work done.

## A Clean start
The first step is to make sure that the SD card is formatted and erased. In most cases the SD card will contain alternative operating systems for the Raspberry Pi. We would like to have a clean start

Go to [The Raspberry Pi homepage](https://www.raspberrypi.org) this is our starting point

* Go to Downloads
    * Read and understand the information
    * Download and install Raspberry Pi imager for your OS
    * Start Raspberry Pi imager

It should look something like this:
![01_pi_imager](../img/itek1/02_pi_imager_start.png)

Test af fork fra hans_eaaa account







